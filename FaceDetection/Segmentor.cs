﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetection
{
    abstract class Segmentor
    {
        public abstract List<Rectangle> DetectFaces(ref RecognizerEngine re, ref Image<Bgr, byte> img, bool drawRectangles, bool isTrainingMode);
    }

    class SegmentorHaar : Segmentor
    {
        private DataStoreAccess _dataStoreAccess;

        public SegmentorHaar()
        {
            _dataStoreAccess = new DataStoreAccess(@"../../data/faces.db");
        }

        public override List<Rectangle> DetectFaces(ref RecognizerEngine re, ref Image<Bgr, byte> img, bool drawRectangles, bool isTrainingMode)
        {
            try
            {
                string facePath = Path.GetFullPath(@"../../data/haarcascade_frontalface_default.xml");

                CascadeClassifier classifierFace = new CascadeClassifier(facePath);
                var imgGray = img.Convert<Gray, byte>().Clone();
                Rectangle[] facesTmp = classifierFace.DetectMultiScale(imgGray, 1.1, 4);

                Image im = imgGray.ToBitmap();

                if (drawRectangles)
                    foreach (var face in facesTmp)
                    {
                        using (var grayImRect = new Bitmap(face.Width, face.Height))
                        {
                            using (var graphics = Graphics.FromImage(grayImRect))
                            {
                                graphics.DrawImage(im, 0.0f, 0.0f, face, GraphicsUnit.Pixel);
                            }
                            //grayImRect.Save(@"../../data/wow.bmp");

                            img.Draw(face, new Bgr(0, 0, 255), 2);
                            if (!isTrainingMode)
                                img.Draw(_dataStoreAccess.GetUsername(re.RecognizeUser(new Image<Gray, byte>(grayImRect))), face.Location, FontFace.HersheyPlain, 5.0, new Bgr(0, 0, 255));
                        }
                    }

                GC.Collect();
                List<Rectangle> faces = facesTmp.ToList();
                return faces;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " | Detect");
                return null;
            }
        }
    }

    class SegmentorVideoFile : Segmentor
    {
        public override List<Rectangle> DetectFaces(ref RecognizerEngine re, ref Image<Bgr, byte> img, bool drawRectangles, bool isTrainingMode)
        {
            try
            {
                string facePath = Path.GetFullPath(@"../../data/haarcascade_frontalface_default.xml");

                CascadeClassifier classifierFace = new CascadeClassifier(facePath);
                var imgGray = img.Convert<Gray, byte>().Clone();
                Rectangle[] facesTmp = classifierFace.DetectMultiScale(imgGray, 1.1, 4);

                if (drawRectangles)
                    foreach (var face in facesTmp)
                    {
                        img.Draw(face, new Bgr(0, 0, 255), 2);
                        img.Draw("Man", face.Location, FontFace.HersheyPlain, 5.0, new Bgr(0, 0, 255));
                    }

                GC.Collect();
                List<Rectangle> faces = facesTmp.ToList();
                return faces;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
    }
}
