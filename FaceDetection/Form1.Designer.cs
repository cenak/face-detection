﻿namespace FaceDetection
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnListOfStudents = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnInfo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnTrain = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSaveImageForTrain = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblDate = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDate2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblProgramMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.cntrSpace = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbGroupListFilter = new System.Windows.Forms.ComboBox();
            this.cbRecognisedFilter = new System.Windows.Forms.ComboBox();
            this.flpListOfStudents = new System.Windows.Forms.FlowLayoutPanel();
            this.btnStatisticOfVisitng = new System.Windows.Forms.Button();
            this.btnCloseContainerListOfStudents = new System.Windows.Forms.Button();
            this.videoBox = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.cntrSpace.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnListOfStudents,
            this.toolStripSeparator1,
            this.btnSettings,
            this.toolStripLabel2,
            this.toolStripSeparator2,
            this.toolStripLabel1,
            this.toolStripSeparator3,
            this.btnInfo,
            this.toolStripSeparator4,
            this.btnTrain,
            this.toolStripSeparator5,
            this.btnSaveImageForTrain});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnListOfStudents
            // 
            this.btnListOfStudents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnListOfStudents.Image = ((System.Drawing.Image)(resources.GetObject("btnListOfStudents.Image")));
            this.btnListOfStudents.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnListOfStudents.Name = "btnListOfStudents";
            this.btnListOfStudents.Size = new System.Drawing.Size(109, 22);
            this.btnListOfStudents.Text = "Список студентов";
            this.btnListOfStudents.Click += new System.EventHandler(this.btnListOfStudents_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnSettings
            // 
            this.btnSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(71, 22);
            this.btnSettings.Text = "Настройки";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(136, 22);
            this.toolStripLabel2.Text = "Внимение студентов: %";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(71, 22);
            this.toolStripLabel1.Text = "На занятии:";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnInfo
            // 
            this.btnInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnInfo.Image")));
            this.btnInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(43, 22);
            this.btnInfo.Text = "Инфо";
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnTrain
            // 
            this.btnTrain.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnTrain.Image = ((System.Drawing.Image)(resources.GetObject("btnTrain.Image")));
            this.btnTrain.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTrain.Name = "btnTrain";
            this.btnTrain.Size = new System.Drawing.Size(37, 22);
            this.btnTrain.Text = "Train";
            this.btnTrain.Click += new System.EventHandler(this.btnTrain_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnSaveImageForTrain
            // 
            this.btnSaveImageForTrain.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSaveImageForTrain.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveImageForTrain.Image")));
            this.btnSaveImageForTrain.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveImageForTrain.Name = "btnSaveImageForTrain";
            this.btnSaveImageForTrain.Size = new System.Drawing.Size(118, 22);
            this.btnSaveImageForTrain.Text = "Save Image for Train";
            this.btnSaveImageForTrain.Click += new System.EventHandler(this.btnSIFT_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblDate,
            this.lblDate2,
            this.lblProgramMode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblDate
            // 
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(0, 17);
            // 
            // lblDate2
            // 
            this.lblDate2.Name = "lblDate2";
            this.lblDate2.Size = new System.Drawing.Size(31, 17);
            this.lblDate2.Text = "Date";
            // 
            // lblProgramMode
            // 
            this.lblProgramMode.Name = "lblProgramMode";
            this.lblProgramMode.Size = new System.Drawing.Size(80, 17);
            this.lblProgramMode.Text = "ProgamMode";
            // 
            // cntrSpace
            // 
            this.cntrSpace.Controls.Add(this.flowLayoutPanel1);
            this.cntrSpace.Controls.Add(this.flpListOfStudents);
            this.cntrSpace.Controls.Add(this.btnStatisticOfVisitng);
            this.cntrSpace.Controls.Add(this.btnCloseContainerListOfStudents);
            this.cntrSpace.Dock = System.Windows.Forms.DockStyle.Left;
            this.cntrSpace.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.cntrSpace.Location = new System.Drawing.Point(0, 25);
            this.cntrSpace.Name = "cntrSpace";
            this.cntrSpace.Size = new System.Drawing.Size(200, 403);
            this.cntrSpace.TabIndex = 4;
            this.cntrSpace.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cbGroupListFilter);
            this.flowLayoutPanel1.Controls.Add(this.cbRecognisedFilter);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(197, 28);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // cbGroupListFilter
            // 
            this.cbGroupListFilter.FormattingEnabled = true;
            this.cbGroupListFilter.Items.AddRange(new object[] {
            "М3О-312Б",
            "М3О-314Б",
            "М3О-321Б"});
            this.cbGroupListFilter.Location = new System.Drawing.Point(3, 3);
            this.cbGroupListFilter.Name = "cbGroupListFilter";
            this.cbGroupListFilter.Size = new System.Drawing.Size(92, 21);
            this.cbGroupListFilter.TabIndex = 2;
            this.cbGroupListFilter.Text = "Группа";
            // 
            // cbRecognisedFilter
            // 
            this.cbRecognisedFilter.FormattingEnabled = true;
            this.cbRecognisedFilter.Items.AddRange(new object[] {
            "Все",
            "Распознанные",
            "Не распознанные"});
            this.cbRecognisedFilter.Location = new System.Drawing.Point(101, 3);
            this.cbRecognisedFilter.Name = "cbRecognisedFilter";
            this.cbRecognisedFilter.Size = new System.Drawing.Size(92, 21);
            this.cbRecognisedFilter.TabIndex = 3;
            this.cbRecognisedFilter.Text = "Распознанные";
            // 
            // flpListOfStudents
            // 
            this.flpListOfStudents.AutoScroll = true;
            this.flpListOfStudents.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flpListOfStudents.Location = new System.Drawing.Point(3, 37);
            this.flpListOfStudents.Name = "flpListOfStudents";
            this.flpListOfStudents.Size = new System.Drawing.Size(193, 293);
            this.flpListOfStudents.TabIndex = 4;
            // 
            // btnStatisticOfVisitng
            // 
            this.btnStatisticOfVisitng.Location = new System.Drawing.Point(3, 336);
            this.btnStatisticOfVisitng.Name = "btnStatisticOfVisitng";
            this.btnStatisticOfVisitng.Size = new System.Drawing.Size(193, 23);
            this.btnStatisticOfVisitng.TabIndex = 5;
            this.btnStatisticOfVisitng.Text = "Статистика посещений";
            this.btnStatisticOfVisitng.UseVisualStyleBackColor = true;
            // 
            // btnCloseContainerListOfStudents
            // 
            this.btnCloseContainerListOfStudents.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseContainerListOfStudents.Location = new System.Drawing.Point(125, 365);
            this.btnCloseContainerListOfStudents.Name = "btnCloseContainerListOfStudents";
            this.btnCloseContainerListOfStudents.Size = new System.Drawing.Size(75, 23);
            this.btnCloseContainerListOfStudents.TabIndex = 1;
            this.btnCloseContainerListOfStudents.Text = "Close";
            this.btnCloseContainerListOfStudents.UseVisualStyleBackColor = true;
            this.btnCloseContainerListOfStudents.Click += new System.EventHandler(this.btnCloseContainerListOfStudents_Click);
            // 
            // videoBox
            // 
            this.videoBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.videoBox.Image = global::FaceDetection.Properties.Resources.TestRoomPhoto;
            this.videoBox.Location = new System.Drawing.Point(0, 25);
            this.videoBox.Name = "videoBox";
            this.videoBox.Size = new System.Drawing.Size(800, 403);
            this.videoBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoBox.TabIndex = 5;
            this.videoBox.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cntrSpace);
            this.Controls.Add(this.videoBox);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "Face Detection";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.cntrSpace.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnListOfStudents;
        private System.Windows.Forms.ToolStripButton btnSettings;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.FlowLayoutPanel cntrSpace;
        private System.Windows.Forms.Button btnCloseContainerListOfStudents;
        private System.Windows.Forms.PictureBox videoBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnInfo;
        private System.Windows.Forms.ToolStripStatusLabel lblDate;
        private System.Windows.Forms.ToolStripStatusLabel lblDate2;
        private System.Windows.Forms.ComboBox cbGroupListFilter;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ComboBox cbRecognisedFilter;
        private System.Windows.Forms.FlowLayoutPanel flpListOfStudents;
        private System.Windows.Forms.Button btnStatisticOfVisitng;
        private System.Windows.Forms.ToolStripStatusLabel lblProgramMode;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnTrain;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton btnSaveImageForTrain;
    }
}

