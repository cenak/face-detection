﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceDetection
{
    interface IDataStoreAccess
    {
        string SaveFace(string username, byte[] faceBlob);

        List<Face> CallFaces(string username);

        bool IsUsernameValid(string username);

        string SaveAdmin(string username, string password);

        bool DeleteUser(string username);

        int GetUserId(string username);

        int GenerateUserId();

        string GetUsername(int userId);

        List<string> GetAllUsernames();
    }
}
