﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetection
{
    public partial class Settings : Form
    {
        MainWindow mainWindow;
        //private 
        OpenFileDialog selectFile = new OpenFileDialog();
        //ShowSavedTrainigData trainingWindow = null;


        public Settings()
        {
            InitializeComponent();
        }

        public Settings(MainWindow mw)
        {
            InitializeComponent();
            mainWindow = mw;
            chbDrawFaces.Checked = MainWindow.drawRectangles;
            cbTrainingMode.Checked = MainWindow.isTrainingMode;
            //cbSelectSource.
        }

        private void Settings_Load(object sender, EventArgs e)
        {

        }

        private void btnSelectVideoFile_Click(object sender, EventArgs e)
        {
            string pathToFile = null;

            if (selectFile.ShowDialog() == DialogResult.OK)
            {
                //string pathToVideoFile = selectVideoFile.FileName;
                pathToFile = selectFile.FileName;
                //pathToFile = pathToFile.Replace("\\", "/");
                //capture = new VideoCapture(pathToVideoFile);
                //MessageBox.Show(pathToVideoFile);
            }

            MainWindow.capture.Pause();
            MainWindow.capture.Dispose();

            MessageBox.Show(pathToFile);

            if (pathToFile != null)
            {
                MainWindow.capture = null;
                MainWindow.capture = new Emgu.CV.VideoCapture(pathToFile);
                MessageBox.Show(MainWindow.capture.CaptureSource.ToString());
                
            }

            MainWindow.capture.Start();
            //MessageBox.Show("Пока не работает :)");
        }

        private void chbDrawFaces_CheckedChanged(object sender, EventArgs e)
        {
            MainWindow.drawRectangles = chbDrawFaces.Checked;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbSelectSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(cbSelectSource.SelectedIndex)
            {
                case 0:
                    //Задаём videocapture конструктор с 0
                    btnSelectVideoFile.Enabled = false;
                    //MainWindow.capture.Stop();
                    MainWindow.capture = new Emgu.CV.VideoCapture();
                    MainWindow.capture.Start();
                    break;

                case 1:
                    btnSelectVideoFile.Enabled = true;
                    //MainWindow.capture.Stop();
                    selectFile.Filter = "MP4 Files(*.mp4)|*.mp4|All Files(*.*)|*.*";
                    break;

                case 2:
                    btnSelectVideoFile.Enabled = true;
                    selectFile.Filter = "JPG Files(*.jpg)|*.jpg|All Files(*.*)|*.*";
                    break;

                default:
                    MessageBox.Show("Something going wrong");
                    break;
            }
        }

        private void cbTrainingMode_CheckedChanged(object sender, EventArgs e)
        {
            MainWindow.isTrainingMode = cbTrainingMode.Checked;
            mainWindow.SetLblProgramMode(cbTrainingMode.Checked);
            mainWindow.EnableOrDisableTrainButton(cbTrainingMode.Checked);
            mainWindow.EnableOrDisableSIFTButton(cbTrainingMode.Checked);
            /*
            if (cbTrainingMode.Checked)
            {
                trainingWindow = new saveTrainigData(this);
                trainingWindow.Show();
            }
            else if (trainingWindow != null)
                trainingWindow.Close();
                */
        }
    }
}
