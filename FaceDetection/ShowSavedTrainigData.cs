﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetection
{
    public partial class ShowSavedTrainigData : Form
    {
        Settings settings;

        public ShowSavedTrainigData()
        {
            InitializeComponent();
        }

        public ShowSavedTrainigData(Settings st)
        {
            InitializeComponent();
            settings = st;
        }

        public ShowSavedTrainigData(Image<Bgr, byte> faceToSave)
        {
            InitializeComponent();
            this.Width = faceToSave.ToBitmap().Width;
            this.Height = faceToSave.ToBitmap().Height;
            pictureBox1.Image = faceToSave.ToBitmap();
        }

        private void TrainingWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            //settings
        }
    }
}
