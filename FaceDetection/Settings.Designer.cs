﻿namespace FaceDetection
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelectVideoFile = new System.Windows.Forms.Button();
            this.chbDrawFaces = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblFramesFrequency = new System.Windows.Forms.Label();
            this.tbFramesFrequency = new System.Windows.Forms.TextBox();
            this.cbSelectSource = new System.Windows.Forms.ComboBox();
            this.cbTrainingMode = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSelectVideoFile
            // 
            this.btnSelectVideoFile.Location = new System.Drawing.Point(110, 81);
            this.btnSelectVideoFile.Name = "btnSelectVideoFile";
            this.btnSelectVideoFile.Size = new System.Drawing.Size(100, 23);
            this.btnSelectVideoFile.TabIndex = 4;
            this.btnSelectVideoFile.Text = "Select Source";
            this.btnSelectVideoFile.UseVisualStyleBackColor = true;
            this.btnSelectVideoFile.Click += new System.EventHandler(this.btnSelectVideoFile_Click);
            // 
            // chbDrawFaces
            // 
            this.chbDrawFaces.AutoSize = true;
            this.chbDrawFaces.Checked = true;
            this.chbDrawFaces.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDrawFaces.Location = new System.Drawing.Point(12, 60);
            this.chbDrawFaces.Name = "chbDrawFaces";
            this.chbDrawFaces.Size = new System.Drawing.Size(115, 17);
            this.chbDrawFaces.TabIndex = 5;
            this.chbDrawFaces.Text = "Отображать лица";
            this.chbDrawFaces.UseVisualStyleBackColor = true;
            this.chbDrawFaces.CheckedChanged += new System.EventHandler(this.chbDrawFaces_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(135, 122);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Закрыть";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblFramesFrequency
            // 
            this.lblFramesFrequency.AutoSize = true;
            this.lblFramesFrequency.Location = new System.Drawing.Point(13, 26);
            this.lblFramesFrequency.Name = "lblFramesFrequency";
            this.lblFramesFrequency.Size = new System.Drawing.Size(91, 13);
            this.lblFramesFrequency.TabIndex = 7;
            this.lblFramesFrequency.Text = "Частота кадров:";
            // 
            // tbFramesFrequency
            // 
            this.tbFramesFrequency.Location = new System.Drawing.Point(110, 23);
            this.tbFramesFrequency.Name = "tbFramesFrequency";
            this.tbFramesFrequency.Size = new System.Drawing.Size(100, 20);
            this.tbFramesFrequency.TabIndex = 8;
            // 
            // cbSelectSource
            // 
            this.cbSelectSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSelectSource.FormattingEnabled = true;
            this.cbSelectSource.Items.AddRange(new object[] {
            "Camera",
            "Video File",
            "Image"});
            this.cbSelectSource.Location = new System.Drawing.Point(12, 83);
            this.cbSelectSource.Name = "cbSelectSource";
            this.cbSelectSource.Size = new System.Drawing.Size(92, 21);
            this.cbSelectSource.TabIndex = 9;
            this.cbSelectSource.SelectedIndexChanged += new System.EventHandler(this.cbSelectSource_SelectedIndexChanged);
            // 
            // cbTrainingMode
            // 
            this.cbTrainingMode.AutoSize = true;
            this.cbTrainingMode.Location = new System.Drawing.Point(11, 126);
            this.cbTrainingMode.Name = "cbTrainingMode";
            this.cbTrainingMode.Size = new System.Drawing.Size(93, 17);
            this.cbTrainingMode.TabIndex = 10;
            this.cbTrainingMode.Text = "Training mode";
            this.cbTrainingMode.UseVisualStyleBackColor = true;
            this.cbTrainingMode.CheckedChanged += new System.EventHandler(this.cbTrainingMode_CheckedChanged);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 157);
            this.Controls.Add(this.cbTrainingMode);
            this.Controls.Add(this.cbSelectSource);
            this.Controls.Add(this.tbFramesFrequency);
            this.Controls.Add(this.lblFramesFrequency);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.chbDrawFaces);
            this.Controls.Add(this.btnSelectVideoFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = " ";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelectVideoFile;
        private System.Windows.Forms.CheckBox chbDrawFaces;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblFramesFrequency;
        private System.Windows.Forms.TextBox tbFramesFrequency;
        private System.Windows.Forms.ComboBox cbSelectSource;
        private System.Windows.Forms.CheckBox cbTrainingMode;
    }
}