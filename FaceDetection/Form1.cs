﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Cvb;
using Emgu.Util;
using System.IO;

namespace FaceDetection
{

    public partial class MainWindow : Form
    {
        public static VideoCapture capture;
        Image<Bgr, byte> img;
        public static bool drawRectangles = true;
        public static bool isTrainingMode = true;
        private RecognizerEngine recognizerEngine = new RecognizerEngine(@"../../data/faces.db", @"../../data/recognizer/recognizer.rce");
        List<Rectangle> faces;

        public MainWindow()
        {
            InitializeComponent();

            if (capture == null)
                capture = new VideoCapture(0);

            this.Width = capture.Width;
            this.Height = capture.Height;

            if (isTrainingMode)
            {
                lblProgramMode.Text = "Training Mode";
                btnTrain.Enabled = true;
                btnSaveImageForTrain.Enabled = true;
            }
            else
            {
                lblProgramMode.Text = "Standart Mode";
                btnTrain.Enabled = false;
                btnSaveImageForTrain.Enabled = false;
            }


            lblDate2.Text = DateTime.Now.ToString();

            capture.ImageGrabbed += Capture_ImageGrabbed;
            capture.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Capture_ImageGrabbed(object sender, EventArgs e)
        {
            Segmentor segmentor = new SegmentorHaar();
            try
            {
                Mat m = new Mat();
                capture.Retrieve(m);
                //Crash here
                //MessageBox.Show("After capture Retireve");
                //videoBox.Image = m.ToImage<Bgr, byte>().Bitmap;
                img = new Image<Bgr, byte>(m.Bitmap);
                //MessageBox.Show("After Img Initialization");

                GC.Collect();
                //MessageBox.Show("After Garbage Collector");

                faces = segmentor.DetectFaces(ref recognizerEngine, ref img, drawRectangles, isTrainingMode);
                //MessageBox.Show("After Segmenter");

                videoBox.Image = img.Bitmap;
                //MessageBox.Show("After VideoBox Initialisation");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message} | Capture_ImageGrabbed | {ex.Data} | {ex.HelpLink}");
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings(this);
            settings.Show(this);
            cntrSpace.Visible = false;
        }

        private void btnListOfStudents_Click(object sender, EventArgs e)
        {
            cntrSpace.Visible = true;
        }

        private void btnCloseContainerListOfStudents_Click(object sender, EventArgs e)
        {
            cntrSpace.Visible = false;
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Source: " + capture.CaptureSource.ToString() +
                            "\nWidth: " + capture.Width.ToString() +
                            "\nHeight: " + capture.Height.ToString() +
                            //"\nFlip type: " + capture.FlipType.ToString() +
                            "\n\nCreators: Kachalin Vasiliy, Chernysheva Anastasiya");
        }

        static void SaveScreenShot(ref Image<Bgr, byte> image)
        {
            image.Save(@"../../data/EducationalScreenshots/" + DateTime.Now.Hour.ToString() + " " + DateTime.Now.Minute.ToString() + " " + DateTime.Now.Second.ToString() + ".bmp");
            
        }

        public void SetLblProgramMode(bool trmode)
        {
            if(trmode)
                lblProgramMode.Text = "Training Mode";
            else
                lblProgramMode.Text = "Standart Mode";
        }

        public void EnableOrDisableTrainButton(bool trmode)
        {
            btnTrain.Enabled = trmode;
        }

        public void EnableOrDisableSIFTButton(bool trmode)
        {
            btnSaveImageForTrain.Enabled = trmode;
        }

        private void btnSIFT_Click(object sender, EventArgs e)
        {
            foreach (var face in faces)
            {
                var imgGray = img.Convert<Gray, byte>().Clone();
                Image im = imgGray.ToBitmap();
                using (var grayImRect = new Bitmap(face.Width, face.Height))
                {
                    using (var graphics = Graphics.FromImage(grayImRect))
                    {
                        graphics.DrawImage(im, 0.0f, 0.0f, face, GraphicsUnit.Pixel);
                    }
                    //grayImRect.Save(@"../../data/wow.bmp");

                    var faceToSave = new Image<Bgr, byte>(grayImRect); //put only rectangle area, not full image

                    byte[] file;
                    IDataStoreAccess dataStore = new DataStoreAccess(@"../../data/faces.db");
                    var frmSaveDialog = new SaveFileDialog();
                    frmSaveDialog.InitialDirectory = @"../../data/training_data";

                    ShowSavedTrainigData showSavedTrainigData = new ShowSavedTrainigData(faceToSave);
                    showSavedTrainigData.Show();

                    if (frmSaveDialog.ShowDialog() == DialogResult.OK)
                    {
                        //MessageBox.Show(Path.GetFileName(frmSaveDialog.FileName));
                        if (frmSaveDialog.FileName.Trim() != string.Empty)
                        {
                            var username = Path.GetFileName(frmSaveDialog.FileName).Trim().ToLower();
                            var filePath = /*Application.StartupPath +*/ string.Format(@"../../data/training_data/{0}.bmp", username);
                            faceToSave.ToBitmap().Save(filePath);
                            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                            {
                                using (var reader = new BinaryReader(stream))
                                {
                                    file = reader.ReadBytes((int)stream.Length);
                                }
                            }
                            var result = dataStore.SaveFace(username, file);
                            MessageBox.Show(result, "Save Result", MessageBoxButtons.OK);
                        }
                    }
                    showSavedTrainigData.Close();
                }
            }
        }

        private void btnTrain_Click(object sender, EventArgs e)
        {
            recognizerEngine.TrainRecognizer();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDate2.Text = DateTime.Now.ToString();
        }
    }
}
